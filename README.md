
##  GitHub

Projeto no GitHub: https://github.com/GabrielBarcelos1/ReactLoginList


## 🚀 Tecnologias

Esse projeto foi desenvolvido com as seguintes tecnologias:

- [React.js]()
- [ContextApi]()
- [React-router-dom]()
- [react-Icons]()
- [Styled-components]()
- [SemanticUI]()
- [Typescript]()
- [react-toastify]()
- [React-testing-library]()
- [json-server]()

## 💻 Projeto
Desenvolvendo esse projeto pratiquei conceitos como  React hooks, troca de informações entre páginas usando contextApi, componentização,responsividade, rotas privadas e publicas com react-router-dom com um sistema de login.

O fluxo do sistema consiste em o usuário logar, ao ele logar ele é redirecionado a uma página com uma lista de pessoas cadastrada nessa página ele tem a opção de adicionar dados de pessoas para complementar o banco de dados, a aplicação inteira é responsiva.

    
## 🔖 Como Executar

#### Clonando o projeto
```sh
git clone https://bitbucket.org/GabrielBarcelos1/reactloginlist/src/master/
cd ReactLoginList
Depois disso é so mecher no projeto.
```


## 🤔 Como contribuir

- Faça um fork desse repositório;
- Cria uma branch com a sua feature: `git checkout -b minha-feature`;
- Faça commit das suas alterações: `git commit -m 'feat: Minha nova feature'`;
- Faça push para a sua branch: `git push origin minha-feature`.

Depois que o merge da sua pull request for feito, você pode deletar a sua branch.


## 🧾 Licença

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE.md) para mais detalhes.

---

<p align="center">Feito com 💗 by Gabriel Vieira Barcelos</p>

